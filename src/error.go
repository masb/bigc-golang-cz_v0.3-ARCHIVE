package bigc

import "C"

import "errors"

//export go_err_str
func go_err_str(err *error, msg *C.char) {
    *err = errors.New(C.GoString(msg))
}
