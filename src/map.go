package bigc

import "C"

//export go_map_make
func go_map_make(m *map[string]interface{}, len C.size_t) {
    *m = make(map[string]interface{}, int(len))
}

//export go_map_set_str
func go_map_set_str(m *map[string]interface{}, key, value *C.char) {
    (*m)[C.GoString(key)] = C.GoString(value)
}
